<html>
    <head>
        <meta http-equiv="content-type" content="text/htm" charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <div class="wrapper_login">
                <div class="left_login">
                    <div class="title_info">
                        <img src="{{ asset('img/svg/kassa.svg')}}">
                        <div class="login_head">
                                MyRas_point 1.0 v
                        </div>
                        <div class="login_content">
                                Все расxоды в одном месте
                        </div>
                    </div>
                </div>
                <div class="right_login">
                    <enter-component></enter-component>
                </div>
            </div>
        </div>
    </body>
</html>
