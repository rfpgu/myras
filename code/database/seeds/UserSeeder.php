<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'AdminDB',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'role'=>'admin'
        ]);

        DB::table('users')->insert([
            'name' => 'user1',
            'email' => 'user1@gmail.com',
            'password' => Hash::make('password'),
            'role'=>'user'
        ]);
    }
}
